import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import Participante from "../../models/Participante";
import { EventoProvider } from "../../providers/evento/evento";
import { HomePage } from '../home/home';

import { Storage } from '@ionic/storage';
import { RegisterPage } from '../register/register';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  email : string = '';
  senha : string = '';

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public toastCtrl : ToastController,
              public eventoProvider : EventoProvider,
              public storage : Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  efetuaLogin() {

    this.eventoProvider.validaUsuario()
    .subscribe(
      resposta => {
        let participantes : Participante[] = Array();
        participantes = resposta;
        participantes = participantes.filter( x => (x.email == this.email && x.senha == this.senha) );
        console.log(participantes);
        if(participantes.length == 1) {
          this.storage.set('usuarioId', participantes[0].id);
          this.storage.set('usuarioNome', participantes[0].nome);
          this.storage.set('usuarioEmail', participantes[0].email);

          localStorage.setItem('lpi',btoa(JSON.stringify(participantes[0])));

          this.navCtrl.setRoot(HomePage);
        }
        else
          this.exibirToast();
      }
    );

  }

  exibirToast() {
    let toast = this.toastCtrl.create({
      message: 'Login/senha inválidos!',
      duration: 2000,
      position: 'bottom'
    });

    toast.present();
  }

  cadastrar(){
    this.navCtrl.push(RegisterPage);
  }
}
