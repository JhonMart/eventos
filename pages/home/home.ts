import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { Storage } from '@ionic/storage';
import { EventoProvider } from '../../providers/evento/evento';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  user = {name: '', email: '', id: null};
  
  eventos;

  minhasInscricoes = [{nome: 'Sem Cursos'}];

  constructor(public navCtrl: NavController, 
              public storage : Storage,
              public eventoProvider : EventoProvider) {
    this.storage.get('usuarioId').then((val) => {
      this.user.id = val;
      this.minhasInscricoes = [];
      this.incricoes(val);
    });

    this.storage.get('usuarioNome').then((val) => {
      this.user.name = val;
    });
    
    this.storage.get('usuarioEmail').then((val) => {
      this.user.email = val;
    });

    this.eventoProvider.getEventos()
    .subscribe(
      resposta => {
        this.eventos = resposta;
      }
    );
  }

  logOff(){
    this.storage.clear();
    this.navCtrl.setRoot(LoginPage);
  }

  incricoes(id){
    this.eventoProvider.getInscricoes()
    .subscribe(
      resposta => {
        this.eventos = resposta.map(i=>{
          if(id==i.idParticipante)
            this.minhasInscricoes.push(this.eventos[i.idEvento-1])
        });
      }
    );
  }
}
