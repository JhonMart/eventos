import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import Evento from '../../models/Evento';
import { EventoProvider } from '../../providers/evento/evento';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';
import Inscricao from '../../models/Inscricao';

@IonicPage()
@Component({
  selector: 'page-evento-detalhe',
  templateUrl: 'evento-detalhe.html',
})
export class EventoDetalhePage {

  evento : Evento;
  incrita = true;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public toastCtrl : ToastController,
              public eventoProvider : EventoProvider,
              public storage : Storage, 
              public alert : AlertController) {
    this.evento = this.navParams.get('eventoSelecionado');
    this.eventoProvider.getInscricoes()
    .subscribe(
      resposta => {
        resposta.map(e=>{
          if(e.idEvento == this.evento.id){
            this.incrita = false;
          }
        });
      }
    );
  }

  ionViewDidLoad() {
    
  }

  inscrever(){
    let idEvento = this.evento.id;
    this.storage.get('usuarioId').then((val) => {
      let idParticipante = val;
      let inscricao = {idParticipante, idEvento};

      this.eventoProvider.inscreverParticipanteEvento(inscricao)
      .subscribe(response => 
        {
          this.navCtrl.setRoot(HomePage);
        },
        erro =>
        {
          this.alert.create(
            {
              title : 'Erro na API',
              buttons : [{text : 'Ok'}],
              subTitle : 'Tente novamente mais tarde!'
            }
          ).present();
        }
      );
    });
  }

  exibirToast(mensagem: string) {
    let toast = this.toastCtrl.create({
      message: mensagem,
      duration: 2000,
      position: 'bottom'
    });

    toast.present();
  }

  desistir(){
    this.eventoProvider.removeInscricoes(this.evento.id)
    .subscribe(response => 
      {
        this.navCtrl.setRoot(HomePage);
      },
      erro =>
      {
        this.exibirToast('Opa deu errado...')
      }
    );
  }
}
