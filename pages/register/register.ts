import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { EventoProvider } from '../../providers/evento/evento';
import Participante from '../../models/Participante';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  participante = {};
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public toastCtrl : ToastController,
              public eventoProvider : EventoProvider) {
  }

  cadastrar(){
    this.eventoProvider.getCadUsuario(this.participante)
      .subscribe(response => 
        {
          this.navCtrl.pop();
        },
        erro =>
        {
          let toast = this.toastCtrl.create({
            message: 'Erro na API',
            duration: 2000,
            position: 'bottom'
          });
          console.log(erro)
          toast.present();
        }
      );
  }

}
