import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import Evento from "../../models/Evento";
import { EventoProvider } from "../../providers/evento/evento";
import { EventoDetalhePage } from '../evento-detalhe/evento-detalhe';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
 
  eventos : Evento[] = Array();
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public eventoProvider : EventoProvider) {

      this.eventoProvider.getEventos()
          .subscribe(
            resposta => this.eventos = resposta
          );
  }

  selecionaEvento(x){
    this.navCtrl.push(EventoDetalhePage, { eventoSelecionado : x });
  }

  inscrever(x){
    console.log('Cadastro no curso: '+x.id)
  }
}
