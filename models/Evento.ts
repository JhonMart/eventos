export default class Evento{
    public id : number;
    public nome : string;
    public tipo : string;
    public local : string;
    public datainicio : string;
    public datafim : string;
    public horainicio : string;
    public horafim : string;
    public vagas : number;
}