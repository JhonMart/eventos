export default class Participante{
    public id :number;
    public nome  : string;
    public telefone  : string;
    public email : string;
    public senha : string;
}