import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Evento from "../../models/Evento";
import Participante from "../../models/Participante";
import Inscricao from '../../models/Inscricao';

@Injectable()
export class EventoProvider {
  urlApi : string = 'http://10.40.20.86:3000/';
  // urlApi : string = 'https://evento-ft.herokuapp.com/';
  public httpOptions;


  constructor(public http: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };
  }

  getEventos(){
    return this.http.get<Evento[]>(this.urlApi + 'eventos/');
  }

  getInscricoes(){
    return this.http.get<Inscricao[]>(this.urlApi + 'inscricoes/');
  }

  removeInscricoes(id){
    return this.http.delete<Inscricao[]>(this.urlApi + 'inscricoes/'+id);
  }

  getCadUsuario(usuario){
    return this.http.post(this.urlApi + 'participantes/', usuario);
  }

  validaUsuario( ){
    return this.http.get<Participante[]>(this.urlApi + 'participantes');
  }

  inscreverParticipanteEvento(i){
    return this.http.post<string>(this.urlApi + 'inscricoes/', i);
  }

}
